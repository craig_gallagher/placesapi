package com.example.juan.placesapi;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by juan on 20/03/16.
 */
public class ApiReader {


    private static final String TAG = "API_READER";
    Context context;
    View v;

    public ApiReader(Context context, View v) {
        this.context = context;
        this.v = v;
    }


    private static final String APIKEY = "AIzaSyANj6qV9bJGYr0i-Cj5u2I4TvOCOvQRqq4";


    private static String ENDPOINT =
            "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=54.27,-8.47&radius=1000&types=car_repair&key=%s";



    // Method to be called
    public void getCarRepairs() throws IOException {
        URL url = new URL(String.format(ENDPOINT, APIKEY));
        new GetPlaces().execute(url);
    }




    // Inner asyctask class
    private class GetPlaces extends AsyncTask<URL, Integer, List<Place>> {
        List<Place> lista;
        @Override
        protected List<Place> doInBackground(URL... params) {
            JSONObject datos;
            URL url = params[0];
            HttpURLConnection urlConnection = null;


            try {
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                datos = Functions.convertInputStreamToJSONObject(in);
                Log.i(TAG, datos.toString());

                // De los datos obtenemos un objeto Place

                lista  = Functions.parsePlacesFromJson(datos, v);
                Log.i(TAG, "" + lista.toString());
                Log.i(TAG, "Went into try was OK.");

            } catch (Exception e) {
                Log.e(TAG, e.toString());
                Log.i(TAG, "Went into catch");

            } finally {
                urlConnection.disconnect();
                Log.i(TAG, "Went into finally, urlConnection disconnected.");
            }

            return lista;
        }


        // This method it gets what the "doInBackGround" returns
        protected void onPostExecute(List<Place> placesList) {

            if(placesList != null) {

                // Do whatever with the list of places
                Functions.updateView(v, placesList.get(0));

            }
        }
    }



}
