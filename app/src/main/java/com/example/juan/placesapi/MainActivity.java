package com.example.juan.placesapi;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.activity_main, null);

        ApiReader api = new ApiReader(this, v);
        try {
            Log.i("ENTRA", "Si");
            api.getCarRepairs();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
