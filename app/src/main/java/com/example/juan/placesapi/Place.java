package com.example.juan.placesapi;

/**
 * Created by juan on 20/03/16.
 */
public class Place {

    private String icon;
    private Double latitude;

    public void setIcon(String icon) {
        this.icon=icon;
    }

    public void setLatitude(Double latitude) {
        this.latitude=latitude;
    }

    public String getIcon() {
        return icon;
    }

    public Double getLatitude() {
        return latitude;
    }
}
